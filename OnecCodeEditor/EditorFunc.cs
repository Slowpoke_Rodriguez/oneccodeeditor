﻿/*
 * Created by SharpDevelop.
 * User: al
 * Date: 14.04.2016
 * Time: 6:47
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;

namespace OnecCodeEditor
{
	/// <summary>
	/// Description of EditorFunc.
	/// </summary>
	public static class EditorFunc
	{	

		public static void closeCurrentTab(System.Windows.Forms.TabControl Tabs_)
		{
			if (Tabs_.SelectedTab.HasChildren)
			{				
				foreach(Control childControl in Tabs_.SelectedTab.Controls)
      			{         			
         			Tabs_.SelectedTab.Controls.Remove(childControl);
      			}
			}				
			Tabs_.TabPages.Remove(Tabs_.SelectedTab);						
		} // closeCurrentTab
		
		
		public static void addNewTab(System.Windows.Forms.TabControl Tabs_,
		                            System.Windows.Forms.ContextMenuStrip contextTabMenu_,
		                            System.Windows.Forms.ContextMenuStrip contextTextTabMenu_)
		{
			int c = Tabs_.TabPages.Count;
			// 1 добавить закладку
			System.Windows.Forms.TabPage tabPage_;
			tabPage_ = new System.Windows.Forms.TabPage();
			tabPage_.ContextMenuStrip = contextTabMenu_;
			tabPage_.Location = new System.Drawing.Point(4, 29);
			tabPage_.Name = "NewTab" + c.ToString();
			tabPage_.Padding = new System.Windows.Forms.Padding(3);
			tabPage_.Size = new System.Drawing.Size(664, 191);
			tabPage_.TabIndex = 0;
			tabPage_.Text = "Новый" + c.ToString();
			tabPage_.UseVisualStyleBackColor = true;
			Tabs_.Controls.Add(tabPage_);
			// 2 добавить richtext на закладку
			System.Windows.Forms.RichTextBox richTextBox_;
			
			richTextBox_ = new System.Windows.Forms.RichTextBox();			
			richTextBox_.ContextMenuStrip = contextTextTabMenu_;
			richTextBox_.Dock = System.Windows.Forms.DockStyle.Fill;
			richTextBox_.Location = new System.Drawing.Point(3, 3);
			richTextBox_.Name = "richTextBox" + c.ToString();
			richTextBox_.Size = new System.Drawing.Size(658, 185);
			richTextBox_.TabIndex = 1;
			richTextBox_.Text = "Новый текст!" + richTextBox_.Name;
			
			tabPage_.Controls.Add(richTextBox_);						
			Tabs_.SelectedTab = tabPage_;			
		} // addNewTab

		public static void openTextFileInNewTab(System.Windows.Forms.TabControl Tabs_,
		                            System.Windows.Forms.ContextMenuStrip contextTabMenu_,
		                            System.Windows.Forms.ContextMenuStrip contextTextTabMenu_,
		                            String FileName_)
		{
			int c = Tabs_.TabPages.Count;
			// 1 добавить закладку
			System.Windows.Forms.TabPage tabPage_;
			tabPage_ = new System.Windows.Forms.TabPage();
			tabPage_.ContextMenuStrip = contextTabMenu_;
			tabPage_.Location = new System.Drawing.Point(4, 29);
			tabPage_.Name = "NewTab" + c.ToString();
			tabPage_.Padding = new System.Windows.Forms.Padding(3);
			tabPage_.Size = new System.Drawing.Size(664, 191);
			tabPage_.TabIndex = 0;
			tabPage_.Text = "Новый" + c.ToString();
			tabPage_.UseVisualStyleBackColor = true;
			Tabs_.Controls.Add(tabPage_);
			// 2 добавить richtext на закладку
			System.Windows.Forms.RichTextBox richTextBox_;
			
			richTextBox_ = new System.Windows.Forms.RichTextBox();			
			richTextBox_.ContextMenuStrip = contextTextTabMenu_;
			richTextBox_.Dock = System.Windows.Forms.DockStyle.Fill;
			richTextBox_.Location = new System.Drawing.Point(3, 3);
			richTextBox_.Name = "richTextBox" + c.ToString();
			richTextBox_.Size = new System.Drawing.Size(658, 185);
			richTextBox_.TabIndex = 1;
			//richTextBox_.Text = "Новый текст!" + richTextBox_.Name;
			richTextBox_.LoadFile(FileName_, RichTextBoxStreamType.PlainText);
			
			
			tabPage_.Controls.Add(richTextBox_);						
			Tabs_.SelectedTab = tabPage_;			
		} // openTextFileInNewTab

		
	} // EditorFunc
} // OnecCodeEditor
