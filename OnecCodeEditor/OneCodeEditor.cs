﻿/*
 * Created by SharpDevelop.
 * User: al
 * Date: 10.04.2016
 * Time: 15:01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace OnecCodeEditor
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class OnecCodeEditor : Form
	{
		private void PopulateTreeView()
     	{
        	TreeNode rootNode;
			DirectoryInfo info = new DirectoryInfo(@"../..");
         	if (info.Exists)
         	{
            	rootNode = new TreeNode(info.Name);
            	rootNode.Tag = info;
            	GetDirectories(info.GetDirectories(), rootNode);
            	treeView1.Nodes.Add(rootNode);
         	}
     	}

     	private void GetDirectories(DirectoryInfo[] subDirs, TreeNode nodeToAddTo)
     	{
        	TreeNode aNode;
        	DirectoryInfo[] subSubDirs;
        	foreach (DirectoryInfo subDir in subDirs)
        	{
            	aNode = new TreeNode(subDir.Name, 0, 0);
            	aNode.Tag = subDir;
				aNode.ImageKey = "folder";
            	subSubDirs = subDir.GetDirectories();
            	if (subSubDirs.Length != 0)
            	{
                	GetDirectories(subSubDirs, aNode);
            	}
            	nodeToAddTo.Nodes.Add(aNode);
         	}
     	}		
     	
		public OnecCodeEditor()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			PopulateTreeView();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		
		#region mainMenu
		
		void ВыходToolStripMenuItemClick(object sender, System.EventArgs e)
		{
			Application.Exit();
			
		} // Выход из программы

		
		void НовыйToolStripMenuItemClick(object sender, EventArgs e)
		{
			addNewTab();
		} // новый файл

		
		void ОткрытьToolStripMenuItemClick(object sender, EventArgs e)
		{
            if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //this.richTextBox1.LoadFile(this.openFileDialog1.FileName, RichTextBoxStreamType.PlainText);
                
                EditorFunc.openTextFileInNewTab(this.tabControl1, 
                                                this.contextTabMenu,
                                                this.contextTextTabMenu,
                                                this.openFileDialog1.FileName);
                //EditorFunc.addNewTab(this.tabControl1, this.contextTabMenu, this.contextTextTabMenu);
                
                //this.tabControl1.SelectedTab.Controls.Find("richTextBox2",true)[0]
            }			
		} // открыть файл
		
		
		void СохранитьToolStripMenuItemClick(object sender, EventArgs e)
		{
            if (this.saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {

                    this.richTextBox1.SaveFile(this.saveFileDialog1.FileName, RichTextBoxStreamType.PlainText);
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }			
		} // сохранить файл
		
				
		void СохранитьКакToolStripMenuItemClick(object sender, EventArgs e)
		{
			MessageBox.Show("сохранить файл как... Ещё не реализовано");
		} // сохранить файл как...
		
				
		void СохранитьВсеToolStripMenuItemClick(object sender, EventArgs e)
		{
			MessageBox.Show("сохранить все Ещё не реализовано");
		} // сохранить все
		
				
		void ПереименоватьToolStripMenuItemClick(object sender, EventArgs e)
		{
			MessageBox.Show("переименовать файл ... Ещё не реализовано");
		} // переименовать файл ...
		
		
		void ЗакрытьToolStripMenuItemClick(object sender, EventArgs e)
		{
			//MessageBox.Show("закрыть текущий файл Ещё не реализовано");
			closeCurrentTab();
			
		} // закрыть текущий файл
		
		
		void ЗакрытьВсеToolStripMenuItemClick(object sender, EventArgs e)
		{
			MessageBox.Show("закрыть вообще все Ещё не реализовано");
		} // закрыть вообще все		
						
		#endregion // mainMenu
		
		#region Methods
		
		/// <summary>
		/// 
		/// </summary>
		private void closeCurrentTab()
		{
			EditorFunc.closeCurrentTab(this.tabControl1);
		} // closeCurrentTab
		
		/// <summary>
		/// Метод 
		/// 1) добавляет новую закладку
		/// 2) добавляет richtext на закладку
		/// </summary>
		private void addNewTab()
		{
			EditorFunc.addNewTab(this.tabControl1, this.contextTabMenu, this.contextTextTabMenu);

		} // addNewTab
		
		
		#endregion // Methods		
		
		#region TabContectMenu
		void НовыйToolStripMenuItem1Click(object sender, EventArgs e)
		{
			addNewTab();
		}
		
		
		void ЗакрытьToolStripMenuItem1Click(object sender, EventArgs e)
		{
			closeCurrentTab();
		}
		#endregion // TabContectMenu
		
		

		#region ToolMainPan
		void ToolMainPanBtnNewFileClick(object sender, EventArgs e)
		{
			addNewTab();
		} // ToolMainPanBtnNewFileClick
		
		
		void ToolMainPanBtnOpenFileClick(object sender, EventArgs e)
		{
			MessageBox.Show("Обработчик  ToolMainPanBtnOpenFileClick Ещё не реализовано");
		} // ToolMainPanBtnOpenFileClick
		
		
		void ToolMainPanBtnSaveFileClick(object sender, EventArgs e)
		{
			MessageBox.Show("Обработчик  ToolMainPanBtnSaveFileClick Ещё не реализовано");
		} // ToolMainPanBtnSaveFileClick
		
		
		void ToolMainPanBtnSaveAllClick(object sender, EventArgs e)
		{
			MessageBox.Show("Обработчик  ToolMainPanBtnSaveAllClick Ещё не реализовано");
		} // ToolMainPanBtnSaveAllClick
		
		
		void ToolMainPanBtnCloseClick(object sender, EventArgs e)
		{
			closeCurrentTab();
		} // ToolMainPanBtnCloseClick
		
		
		
		void ToolMainPanBtnCloseAllClick(object sender, EventArgs e)
		{
			MessageBox.Show("Обработчик ToolMainPanBtnCloseAllClick Ещё не реализовано");
		} // ToolMainPanBtnCloseAllClick
		
		#endregion // ToolMainPan
	} // class OnecCodeEditor : Form
	
} // namespace OnecCodeEditor
