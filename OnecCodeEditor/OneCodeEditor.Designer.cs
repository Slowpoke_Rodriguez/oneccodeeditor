﻿/*
 * Created by SharpDevelop.
 * User: al
 * Date: 10.04.2016
 * Time: 15:01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace OnecCodeEditor
{
	partial class OnecCodeEditor
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/*
		private void Initialize_mainMenu()
		{
		} // Initialize_mainMenu
		*/
		
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnecCodeEditor));
			this.mainMenu = new System.Windows.Forms.MenuStrip();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.новыйToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.сохранитьКакToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.сохранитьВсеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.переименоватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.закрытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.закрытьВсеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.отменитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.повторитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.вырезатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.копироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.вставитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.выделитьВсеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.приблизитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.отдалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.висстановитьВидToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.projectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.новыйЭлементToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.существующийЭлементToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.новаяПапкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.существующаяПапкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.разборкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.сборкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.unPackv8ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.платформаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.buildToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.выполнитьВКонсолиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.выполнитьНаПлатформе1сToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.отладкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.найтиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.найтиВФайлахToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.следующийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.предыдущийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.заменитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.закладкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.настройкиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.readmeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
			this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.toolMainPanBtnNewFile = new System.Windows.Forms.ToolStripButton();
			this.toolMainPanBtnOpenFile = new System.Windows.Forms.ToolStripButton();
			this.ToolMainPanBtnSaveFile = new System.Windows.Forms.ToolStripButton();
			this.ToolMainPanBtnSaveAll = new System.Windows.Forms.ToolStripButton();
			this.ToolMainPanBtnClose = new System.Windows.Forms.ToolStripButton();
			this.ToolMainPanBtnCloseAll = new System.Windows.Forms.ToolStripButton();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.contextTreeViewMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.добавитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.contextTabMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.новыйToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.закрытьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.закрытьВсеКромеТекущейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.contextTextTabMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.отменитьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.повторитьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.копироватьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.вырезатьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.вставитьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.удалитьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.tabControl2 = new System.Windows.Forms.TabControl();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.fastColoredTextBox1 = new FastColoredTextBoxNS.FastColoredTextBox();
			this.mainMenu.SuspendLayout();
			this.toolStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.contextTreeViewMenu.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.contextTabMenu.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.contextTextTabMenu.SuspendLayout();
			this.tabControl2.SuspendLayout();
			this.tabPage3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fastColoredTextBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// mainMenu
			// 
			this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.toolStripMenuItem1,
									this.editToolStripMenuItem,
									this.viewToolStripMenuItem,
									this.projectToolStripMenuItem,
									this.разборкаToolStripMenuItem,
									this.сборкаToolStripMenuItem,
									this.buildToolStripMenuItem,
									this.отладкаToolStripMenuItem,
									this.searchToolStripMenuItem,
									this.настройкиToolStripMenuItem1,
									this.toolsToolStripMenuItem,
									this.helpToolStripMenuItem});
			this.mainMenu.Location = new System.Drawing.Point(0, 0);
			this.mainMenu.Name = "mainMenu";
			this.mainMenu.Size = new System.Drawing.Size(1014, 29);
			this.mainMenu.TabIndex = 0;
			this.mainMenu.Text = "menuStrip1";
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.новыйToolStripMenuItem,
									this.открытьToolStripMenuItem,
									this.сохранитьToolStripMenuItem,
									this.сохранитьКакToolStripMenuItem,
									this.сохранитьВсеToolStripMenuItem,
									this.переименоватьToolStripMenuItem,
									this.закрытьToolStripMenuItem,
									this.закрытьВсеToolStripMenuItem,
									this.toolStripSeparator1,
									this.выходToolStripMenuItem});
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(63, 25);
			this.toolStripMenuItem1.Text = "&Файл";
			// 
			// новыйToolStripMenuItem
			// 
			this.новыйToolStripMenuItem.Name = "новыйToolStripMenuItem";
			this.новыйToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
			this.новыйToolStripMenuItem.Text = "Новый";
			this.новыйToolStripMenuItem.Click += new System.EventHandler(this.НовыйToolStripMenuItemClick);
			// 
			// открытьToolStripMenuItem
			// 
			this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
			this.открытьToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
			this.открытьToolStripMenuItem.Text = "Открыть...";
			this.открытьToolStripMenuItem.Click += new System.EventHandler(this.ОткрытьToolStripMenuItemClick);
			// 
			// сохранитьToolStripMenuItem
			// 
			this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
			this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
			this.сохранитьToolStripMenuItem.Text = "Сохранить";
			this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.СохранитьToolStripMenuItemClick);
			// 
			// сохранитьКакToolStripMenuItem
			// 
			this.сохранитьКакToolStripMenuItem.Name = "сохранитьКакToolStripMenuItem";
			this.сохранитьКакToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
			this.сохранитьКакToolStripMenuItem.Text = "Сохранить как...";
			this.сохранитьКакToolStripMenuItem.Click += new System.EventHandler(this.СохранитьКакToolStripMenuItemClick);
			// 
			// сохранитьВсеToolStripMenuItem
			// 
			this.сохранитьВсеToolStripMenuItem.Name = "сохранитьВсеToolStripMenuItem";
			this.сохранитьВсеToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
			this.сохранитьВсеToolStripMenuItem.Text = "Сохранить все";
			this.сохранитьВсеToolStripMenuItem.Click += new System.EventHandler(this.СохранитьВсеToolStripMenuItemClick);
			// 
			// переименоватьToolStripMenuItem
			// 
			this.переименоватьToolStripMenuItem.Name = "переименоватьToolStripMenuItem";
			this.переименоватьToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
			this.переименоватьToolStripMenuItem.Text = "Переименовать...";
			this.переименоватьToolStripMenuItem.Click += new System.EventHandler(this.ПереименоватьToolStripMenuItemClick);
			// 
			// закрытьToolStripMenuItem
			// 
			this.закрытьToolStripMenuItem.Name = "закрытьToolStripMenuItem";
			this.закрытьToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
			this.закрытьToolStripMenuItem.Text = "Закрыть";
			this.закрытьToolStripMenuItem.Click += new System.EventHandler(this.ЗакрытьToolStripMenuItemClick);
			// 
			// закрытьВсеToolStripMenuItem
			// 
			this.закрытьВсеToolStripMenuItem.Name = "закрытьВсеToolStripMenuItem";
			this.закрытьВсеToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
			this.закрытьВсеToolStripMenuItem.Text = "Закрыть все";
			this.закрытьВсеToolStripMenuItem.Click += new System.EventHandler(this.ЗакрытьВсеToolStripMenuItemClick);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(214, 6);
			// 
			// выходToolStripMenuItem
			// 
			this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
			this.выходToolStripMenuItem.Size = new System.Drawing.Size(217, 26);
			this.выходToolStripMenuItem.Text = "Выход";
			this.выходToolStripMenuItem.Click += new System.EventHandler(this.ВыходToolStripMenuItemClick);
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.отменитьToolStripMenuItem,
									this.повторитьToolStripMenuItem,
									this.toolStripSeparator2,
									this.вырезатьToolStripMenuItem,
									this.копироватьToolStripMenuItem,
									this.вставитьToolStripMenuItem,
									this.удалитьToolStripMenuItem,
									this.выделитьВсеToolStripMenuItem});
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(92, 25);
			this.editToolStripMenuItem.Text = "&Редактор";
			// 
			// отменитьToolStripMenuItem
			// 
			this.отменитьToolStripMenuItem.Name = "отменитьToolStripMenuItem";
			this.отменитьToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
			this.отменитьToolStripMenuItem.Text = "Отменить";
			// 
			// повторитьToolStripMenuItem
			// 
			this.повторитьToolStripMenuItem.Name = "повторитьToolStripMenuItem";
			this.повторитьToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
			this.повторитьToolStripMenuItem.Text = "Повторить";
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(184, 6);
			// 
			// вырезатьToolStripMenuItem
			// 
			this.вырезатьToolStripMenuItem.Name = "вырезатьToolStripMenuItem";
			this.вырезатьToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
			this.вырезатьToolStripMenuItem.Text = "Вырезать";
			// 
			// копироватьToolStripMenuItem
			// 
			this.копироватьToolStripMenuItem.Name = "копироватьToolStripMenuItem";
			this.копироватьToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
			this.копироватьToolStripMenuItem.Text = "Копировать";
			// 
			// вставитьToolStripMenuItem
			// 
			this.вставитьToolStripMenuItem.Name = "вставитьToolStripMenuItem";
			this.вставитьToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
			this.вставитьToolStripMenuItem.Text = "Вставить";
			// 
			// удалитьToolStripMenuItem
			// 
			this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
			this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
			this.удалитьToolStripMenuItem.Text = "Удалить";
			// 
			// выделитьВсеToolStripMenuItem
			// 
			this.выделитьВсеToolStripMenuItem.Name = "выделитьВсеToolStripMenuItem";
			this.выделитьВсеToolStripMenuItem.Size = new System.Drawing.Size(187, 26);
			this.выделитьВсеToolStripMenuItem.Text = "Выделить все";
			// 
			// viewToolStripMenuItem
			// 
			this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.приблизитьToolStripMenuItem,
									this.отдалитьToolStripMenuItem,
									this.висстановитьВидToolStripMenuItem});
			this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
			this.viewToolStripMenuItem.Size = new System.Drawing.Size(51, 25);
			this.viewToolStripMenuItem.Text = "&Вид";
			// 
			// приблизитьToolStripMenuItem
			// 
			this.приблизитьToolStripMenuItem.Name = "приблизитьToolStripMenuItem";
			this.приблизитьToolStripMenuItem.Size = new System.Drawing.Size(220, 26);
			this.приблизитьToolStripMenuItem.Text = "Приблизить";
			// 
			// отдалитьToolStripMenuItem
			// 
			this.отдалитьToolStripMenuItem.Name = "отдалитьToolStripMenuItem";
			this.отдалитьToolStripMenuItem.Size = new System.Drawing.Size(220, 26);
			this.отдалитьToolStripMenuItem.Text = "Отдалить";
			// 
			// висстановитьВидToolStripMenuItem
			// 
			this.висстановитьВидToolStripMenuItem.Name = "висстановитьВидToolStripMenuItem";
			this.висстановитьВидToolStripMenuItem.Size = new System.Drawing.Size(220, 26);
			this.висстановитьВидToolStripMenuItem.Text = "Восстановить вид";
			// 
			// projectToolStripMenuItem
			// 
			this.projectToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.новыйЭлементToolStripMenuItem,
									this.существующийЭлементToolStripMenuItem,
									this.новаяПапкаToolStripMenuItem,
									this.существующаяПапкаToolStripMenuItem,
									this.toolStripSeparator3,
									this.настройкиToolStripMenuItem});
			this.projectToolStripMenuItem.Name = "projectToolStripMenuItem";
			this.projectToolStripMenuItem.Size = new System.Drawing.Size(76, 25);
			this.projectToolStripMenuItem.Text = "&Проект";
			// 
			// новыйЭлементToolStripMenuItem
			// 
			this.новыйЭлементToolStripMenuItem.Name = "новыйЭлементToolStripMenuItem";
			this.новыйЭлементToolStripMenuItem.Size = new System.Drawing.Size(283, 26);
			this.новыйЭлементToolStripMenuItem.Text = "Новый элемент...";
			// 
			// существующийЭлементToolStripMenuItem
			// 
			this.существующийЭлементToolStripMenuItem.Name = "существующийЭлементToolStripMenuItem";
			this.существующийЭлементToolStripMenuItem.Size = new System.Drawing.Size(283, 26);
			this.существующийЭлементToolStripMenuItem.Text = "Существующий элемент...";
			// 
			// новаяПапкаToolStripMenuItem
			// 
			this.новаяПапкаToolStripMenuItem.Name = "новаяПапкаToolStripMenuItem";
			this.новаяПапкаToolStripMenuItem.Size = new System.Drawing.Size(283, 26);
			this.новаяПапкаToolStripMenuItem.Text = "Новая папка ...";
			// 
			// существующаяПапкаToolStripMenuItem
			// 
			this.существующаяПапкаToolStripMenuItem.Name = "существующаяПапкаToolStripMenuItem";
			this.существующаяПапкаToolStripMenuItem.Size = new System.Drawing.Size(283, 26);
			this.существующаяПапкаToolStripMenuItem.Text = "Существующая папка...";
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(280, 6);
			// 
			// настройкиToolStripMenuItem
			// 
			this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
			this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(283, 26);
			this.настройкиToolStripMenuItem.Text = "Настройки...";
			// 
			// разборкаToolStripMenuItem
			// 
			this.разборкаToolStripMenuItem.Name = "разборкаToolStripMenuItem";
			this.разборкаToolStripMenuItem.Size = new System.Drawing.Size(92, 25);
			this.разборкаToolStripMenuItem.Text = "Разборка";
			// 
			// сборкаToolStripMenuItem
			// 
			this.сборкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.unPackv8ToolStripMenuItem,
									this.платформаToolStripMenuItem});
			this.сборкаToolStripMenuItem.Name = "сборкаToolStripMenuItem";
			this.сборкаToolStripMenuItem.Size = new System.Drawing.Size(76, 25);
			this.сборкаToolStripMenuItem.Text = "Сборка";
			// 
			// unPackv8ToolStripMenuItem
			// 
			this.unPackv8ToolStripMenuItem.Name = "unPackv8ToolStripMenuItem";
			this.unPackv8ToolStripMenuItem.Size = new System.Drawing.Size(168, 26);
			this.unPackv8ToolStripMenuItem.Text = "UnPackv8";
			// 
			// платформаToolStripMenuItem
			// 
			this.платформаToolStripMenuItem.Name = "платформаToolStripMenuItem";
			this.платформаToolStripMenuItem.Size = new System.Drawing.Size(168, 26);
			this.платформаToolStripMenuItem.Text = "Платформа";
			// 
			// buildToolStripMenuItem
			// 
			this.buildToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.выполнитьВКонсолиToolStripMenuItem,
									this.выполнитьНаПлатформе1сToolStripMenuItem});
			this.buildToolStripMenuItem.Name = "buildToolStripMenuItem";
			this.buildToolStripMenuItem.Size = new System.Drawing.Size(74, 25);
			this.buildToolStripMenuItem.Text = "&Запуск";
			// 
			// выполнитьВКонсолиToolStripMenuItem
			// 
			this.выполнитьВКонсолиToolStripMenuItem.Name = "выполнитьВКонсолиToolStripMenuItem";
			this.выполнитьВКонсолиToolStripMenuItem.Size = new System.Drawing.Size(192, 26);
			this.выполнитьВКонсолиToolStripMenuItem.Text = "В консоли";
			// 
			// выполнитьНаПлатформе1сToolStripMenuItem
			// 
			this.выполнитьНаПлатформе1сToolStripMenuItem.Name = "выполнитьНаПлатформе1сToolStripMenuItem";
			this.выполнитьНаПлатформе1сToolStripMenuItem.Size = new System.Drawing.Size(192, 26);
			this.выполнитьНаПлатформе1сToolStripMenuItem.Text = "На платформе";
			// 
			// отладкаToolStripMenuItem
			// 
			this.отладкаToolStripMenuItem.Name = "отладкаToolStripMenuItem";
			this.отладкаToolStripMenuItem.Size = new System.Drawing.Size(86, 25);
			this.отладкаToolStripMenuItem.Text = "Отладка";
			// 
			// searchToolStripMenuItem
			// 
			this.searchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.найтиToolStripMenuItem,
									this.найтиВФайлахToolStripMenuItem,
									this.следующийToolStripMenuItem,
									this.предыдущийToolStripMenuItem,
									this.заменитьToolStripMenuItem,
									this.toolStripSeparator4,
									this.закладкиToolStripMenuItem});
			this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
			this.searchToolStripMenuItem.Size = new System.Drawing.Size(68, 25);
			this.searchToolStripMenuItem.Text = "&Поиск";
			// 
			// найтиToolStripMenuItem
			// 
			this.найтиToolStripMenuItem.Name = "найтиToolStripMenuItem";
			this.найтиToolStripMenuItem.Size = new System.Drawing.Size(220, 26);
			this.найтиToolStripMenuItem.Text = "Найти...";
			// 
			// найтиВФайлахToolStripMenuItem
			// 
			this.найтиВФайлахToolStripMenuItem.Name = "найтиВФайлахToolStripMenuItem";
			this.найтиВФайлахToolStripMenuItem.Size = new System.Drawing.Size(220, 26);
			this.найтиВФайлахToolStripMenuItem.Text = "Найти в файлах...";
			// 
			// следующийToolStripMenuItem
			// 
			this.следующийToolStripMenuItem.Name = "следующийToolStripMenuItem";
			this.следующийToolStripMenuItem.Size = new System.Drawing.Size(220, 26);
			this.следующийToolStripMenuItem.Text = "Следующий";
			// 
			// предыдущийToolStripMenuItem
			// 
			this.предыдущийToolStripMenuItem.Name = "предыдущийToolStripMenuItem";
			this.предыдущийToolStripMenuItem.Size = new System.Drawing.Size(220, 26);
			this.предыдущийToolStripMenuItem.Text = "Предыдущий";
			// 
			// заменитьToolStripMenuItem
			// 
			this.заменитьToolStripMenuItem.Name = "заменитьToolStripMenuItem";
			this.заменитьToolStripMenuItem.Size = new System.Drawing.Size(220, 26);
			this.заменитьToolStripMenuItem.Text = "Заменить...";
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(217, 6);
			// 
			// закладкиToolStripMenuItem
			// 
			this.закладкиToolStripMenuItem.Name = "закладкиToolStripMenuItem";
			this.закладкиToolStripMenuItem.Size = new System.Drawing.Size(220, 26);
			this.закладкиToolStripMenuItem.Text = "Закладки";
			// 
			// настройкиToolStripMenuItem1
			// 
			this.настройкиToolStripMenuItem1.Name = "настройкиToolStripMenuItem1";
			this.настройкиToolStripMenuItem1.Size = new System.Drawing.Size(104, 25);
			this.настройкиToolStripMenuItem1.Text = "Настройки";
			// 
			// toolsToolStripMenuItem
			// 
			this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
			this.toolsToolStripMenuItem.Size = new System.Drawing.Size(126, 25);
			this.toolsToolStripMenuItem.Text = "&Инструменты";
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.readmeToolStripMenuItem,
									this.toolStripSeparator5,
									this.оПрограммеToolStripMenuItem});
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(84, 25);
			this.helpToolStripMenuItem.Text = "&Помощь";
			// 
			// readmeToolStripMenuItem
			// 
			this.readmeToolStripMenuItem.Name = "readmeToolStripMenuItem";
			this.readmeToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
			this.readmeToolStripMenuItem.Text = "Readme";
			// 
			// toolStripSeparator5
			// 
			this.toolStripSeparator5.Name = "toolStripSeparator5";
			this.toolStripSeparator5.Size = new System.Drawing.Size(193, 6);
			// 
			// оПрограммеToolStripMenuItem
			// 
			this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
			this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(196, 26);
			this.оПрограммеToolStripMenuItem.Text = "О программе...";
			// 
			// toolStrip1
			// 
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.toolMainPanBtnNewFile,
									this.toolMainPanBtnOpenFile,
									this.ToolMainPanBtnSaveFile,
									this.ToolMainPanBtnSaveAll,
									this.ToolMainPanBtnClose,
									this.ToolMainPanBtnCloseAll});
			this.toolStrip1.Location = new System.Drawing.Point(0, 29);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(1014, 25);
			this.toolStrip1.TabIndex = 1;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// toolMainPanBtnNewFile
			// 
			this.toolMainPanBtnNewFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolMainPanBtnNewFile.Image = ((System.Drawing.Image)(resources.GetObject("toolMainPanBtnNewFile.Image")));
			this.toolMainPanBtnNewFile.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolMainPanBtnNewFile.Name = "toolMainPanBtnNewFile";
			this.toolMainPanBtnNewFile.Size = new System.Drawing.Size(23, 22);
			this.toolMainPanBtnNewFile.Text = "Новый";
			this.toolMainPanBtnNewFile.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
			this.toolMainPanBtnNewFile.Click += new System.EventHandler(this.ToolMainPanBtnNewFileClick);
			// 
			// toolMainPanBtnOpenFile
			// 
			this.toolMainPanBtnOpenFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolMainPanBtnOpenFile.Image = ((System.Drawing.Image)(resources.GetObject("toolMainPanBtnOpenFile.Image")));
			this.toolMainPanBtnOpenFile.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolMainPanBtnOpenFile.Name = "toolMainPanBtnOpenFile";
			this.toolMainPanBtnOpenFile.Size = new System.Drawing.Size(23, 22);
			this.toolMainPanBtnOpenFile.Text = "Открыть";
			this.toolMainPanBtnOpenFile.Click += new System.EventHandler(this.ToolMainPanBtnOpenFileClick);
			// 
			// ToolMainPanBtnSaveFile
			// 
			this.ToolMainPanBtnSaveFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ToolMainPanBtnSaveFile.Image = ((System.Drawing.Image)(resources.GetObject("ToolMainPanBtnSaveFile.Image")));
			this.ToolMainPanBtnSaveFile.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ToolMainPanBtnSaveFile.Name = "ToolMainPanBtnSaveFile";
			this.ToolMainPanBtnSaveFile.Size = new System.Drawing.Size(23, 22);
			this.ToolMainPanBtnSaveFile.Text = "Сохранить";
			this.ToolMainPanBtnSaveFile.Click += new System.EventHandler(this.ToolMainPanBtnSaveFileClick);
			// 
			// ToolMainPanBtnSaveAll
			// 
			this.ToolMainPanBtnSaveAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ToolMainPanBtnSaveAll.Image = ((System.Drawing.Image)(resources.GetObject("ToolMainPanBtnSaveAll.Image")));
			this.ToolMainPanBtnSaveAll.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ToolMainPanBtnSaveAll.Name = "ToolMainPanBtnSaveAll";
			this.ToolMainPanBtnSaveAll.Size = new System.Drawing.Size(23, 22);
			this.ToolMainPanBtnSaveAll.Text = "Сохранить все";
			this.ToolMainPanBtnSaveAll.Click += new System.EventHandler(this.ToolMainPanBtnSaveAllClick);
			// 
			// ToolMainPanBtnClose
			// 
			this.ToolMainPanBtnClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ToolMainPanBtnClose.Image = ((System.Drawing.Image)(resources.GetObject("ToolMainPanBtnClose.Image")));
			this.ToolMainPanBtnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ToolMainPanBtnClose.Name = "ToolMainPanBtnClose";
			this.ToolMainPanBtnClose.Size = new System.Drawing.Size(23, 22);
			this.ToolMainPanBtnClose.Text = "Закрыть";
			this.ToolMainPanBtnClose.Click += new System.EventHandler(this.ToolMainPanBtnCloseClick);
			// 
			// ToolMainPanBtnCloseAll
			// 
			this.ToolMainPanBtnCloseAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ToolMainPanBtnCloseAll.Image = ((System.Drawing.Image)(resources.GetObject("ToolMainPanBtnCloseAll.Image")));
			this.ToolMainPanBtnCloseAll.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ToolMainPanBtnCloseAll.Name = "ToolMainPanBtnCloseAll";
			this.ToolMainPanBtnCloseAll.Size = new System.Drawing.Size(23, 22);
			this.ToolMainPanBtnCloseAll.Text = "Закрыть все";
			this.ToolMainPanBtnCloseAll.Click += new System.EventHandler(this.ToolMainPanBtnCloseAllClick);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Location = new System.Drawing.Point(0, 708);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(1014, 22);
			this.statusStrip1.TabIndex = 2;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 54);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.treeView1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
			this.splitContainer1.Size = new System.Drawing.Size(1014, 654);
			this.splitContainer1.SplitterDistance = 160;
			this.splitContainer1.TabIndex = 3;
			// 
			// treeView1
			// 
			this.treeView1.ContextMenuStrip = this.contextTreeViewMenu;
			this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView1.Location = new System.Drawing.Point(0, 0);
			this.treeView1.Name = "treeView1";
			this.treeView1.Size = new System.Drawing.Size(160, 654);
			this.treeView1.TabIndex = 0;
			this.treeView1.Tag = "";
			// 
			// contextTreeViewMenu
			// 
			this.contextTreeViewMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.добавитьToolStripMenuItem});
			this.contextTreeViewMenu.Name = "contextMenuStrip3";
			this.contextTreeViewMenu.Size = new System.Drawing.Size(171, 30);
			// 
			// добавитьToolStripMenuItem
			// 
			this.добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
			this.добавитьToolStripMenuItem.Size = new System.Drawing.Size(170, 26);
			this.добавитьToolStripMenuItem.Text = "Добавить...";
			// 
			// splitContainer2
			// 
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.Location = new System.Drawing.Point(0, 0);
			this.splitContainer2.Name = "splitContainer2";
			this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.Controls.Add(this.tabControl1);
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.Controls.Add(this.tabControl2);
			this.splitContainer2.Size = new System.Drawing.Size(850, 654);
			this.splitContainer2.SplitterDistance = 524;
			this.splitContainer2.TabIndex = 0;
			// 
			// tabControl1
			// 
			this.tabControl1.ContextMenuStrip = this.contextTabMenu;
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(850, 524);
			this.tabControl1.TabIndex = 0;
			// 
			// contextTabMenu
			// 
			this.contextTabMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.новыйToolStripMenuItem1,
									this.закрытьToolStripMenuItem1,
									this.закрытьВсеКромеТекущейToolStripMenuItem});
			this.contextTabMenu.Name = "contextMenuStrip2";
			this.contextTabMenu.Size = new System.Drawing.Size(297, 82);
			// 
			// новыйToolStripMenuItem1
			// 
			this.новыйToolStripMenuItem1.Name = "новыйToolStripMenuItem1";
			this.новыйToolStripMenuItem1.Size = new System.Drawing.Size(296, 26);
			this.новыйToolStripMenuItem1.Text = "Новый";
			this.новыйToolStripMenuItem1.Click += new System.EventHandler(this.НовыйToolStripMenuItem1Click);
			// 
			// закрытьToolStripMenuItem1
			// 
			this.закрытьToolStripMenuItem1.Name = "закрытьToolStripMenuItem1";
			this.закрытьToolStripMenuItem1.Size = new System.Drawing.Size(296, 26);
			this.закрытьToolStripMenuItem1.Text = "Закрыть";
			this.закрытьToolStripMenuItem1.Click += new System.EventHandler(this.ЗакрытьToolStripMenuItem1Click);
			// 
			// закрытьВсеКромеТекущейToolStripMenuItem
			// 
			this.закрытьВсеКромеТекущейToolStripMenuItem.Name = "закрытьВсеКромеТекущейToolStripMenuItem";
			this.закрытьВсеКромеТекущейToolStripMenuItem.Size = new System.Drawing.Size(296, 26);
			this.закрытьВсеКромеТекущейToolStripMenuItem.Text = "Закрыть все кроме текущей";
			// 
			// tabPage1
			// 
			this.tabPage1.ContextMenuStrip = this.contextTabMenu;
			this.tabPage1.Controls.Add(this.fastColoredTextBox1);
			this.tabPage1.Location = new System.Drawing.Point(4, 29);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(842, 491);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Браузер";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.richTextBox1);
			this.tabPage2.Location = new System.Drawing.Point(4, 29);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(842, 491);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Текст";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// richTextBox1
			// 
			this.richTextBox1.ContextMenuStrip = this.contextTextTabMenu;
			this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.richTextBox1.Location = new System.Drawing.Point(3, 3);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.Size = new System.Drawing.Size(836, 485);
			this.richTextBox1.TabIndex = 1;
			this.richTextBox1.Text = "";
			// 
			// contextTextTabMenu
			// 
			this.contextTextTabMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
									this.отменитьToolStripMenuItem1,
									this.повторитьToolStripMenuItem1,
									this.копироватьToolStripMenuItem1,
									this.вырезатьToolStripMenuItem1,
									this.вставитьToolStripMenuItem1,
									this.удалитьToolStripMenuItem1});
			this.contextTextTabMenu.Name = "contextTextTabMenu";
			this.contextTextTabMenu.Size = new System.Drawing.Size(173, 160);
			// 
			// отменитьToolStripMenuItem1
			// 
			this.отменитьToolStripMenuItem1.Name = "отменитьToolStripMenuItem1";
			this.отменитьToolStripMenuItem1.Size = new System.Drawing.Size(172, 26);
			this.отменитьToolStripMenuItem1.Text = "Отменить";
			// 
			// повторитьToolStripMenuItem1
			// 
			this.повторитьToolStripMenuItem1.Name = "повторитьToolStripMenuItem1";
			this.повторитьToolStripMenuItem1.Size = new System.Drawing.Size(172, 26);
			this.повторитьToolStripMenuItem1.Text = "Повторить";
			// 
			// копироватьToolStripMenuItem1
			// 
			this.копироватьToolStripMenuItem1.Name = "копироватьToolStripMenuItem1";
			this.копироватьToolStripMenuItem1.Size = new System.Drawing.Size(172, 26);
			this.копироватьToolStripMenuItem1.Text = "Копировать";
			// 
			// вырезатьToolStripMenuItem1
			// 
			this.вырезатьToolStripMenuItem1.Name = "вырезатьToolStripMenuItem1";
			this.вырезатьToolStripMenuItem1.Size = new System.Drawing.Size(172, 26);
			this.вырезатьToolStripMenuItem1.Text = "Вырезать";
			// 
			// вставитьToolStripMenuItem1
			// 
			this.вставитьToolStripMenuItem1.Name = "вставитьToolStripMenuItem1";
			this.вставитьToolStripMenuItem1.Size = new System.Drawing.Size(172, 26);
			this.вставитьToolStripMenuItem1.Text = "Вставить";
			// 
			// удалитьToolStripMenuItem1
			// 
			this.удалитьToolStripMenuItem1.Name = "удалитьToolStripMenuItem1";
			this.удалитьToolStripMenuItem1.Size = new System.Drawing.Size(172, 26);
			this.удалитьToolStripMenuItem1.Text = "Удалить";
			// 
			// tabControl2
			// 
			this.tabControl2.Alignment = System.Windows.Forms.TabAlignment.Bottom;
			this.tabControl2.Controls.Add(this.tabPage3);
			this.tabControl2.Controls.Add(this.tabPage4);
			this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl2.Location = new System.Drawing.Point(0, 0);
			this.tabControl2.Name = "tabControl2";
			this.tabControl2.SelectedIndex = 0;
			this.tabControl2.Size = new System.Drawing.Size(850, 126);
			this.tabControl2.TabIndex = 0;
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.textBox1);
			this.tabPage3.Location = new System.Drawing.Point(4, 4);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(842, 93);
			this.tabPage3.TabIndex = 0;
			this.tabPage3.Text = "Вывод";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// textBox1
			// 
			this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBox1.Location = new System.Drawing.Point(3, 3);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(836, 87);
			this.textBox1.TabIndex = 0;
			// 
			// tabPage4
			// 
			this.tabPage4.Location = new System.Drawing.Point(4, 4);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage4.Size = new System.Drawing.Size(842, 93);
			this.tabPage4.TabIndex = 1;
			this.tabPage4.Text = "tabPage4";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			// 
			// fastColoredTextBox1
			// 
			this.fastColoredTextBox1.AutoCompleteBracketsList = new char[] {
						'(',
						')',
						'{',
						'}',
						'[',
						']',
						'\"',
						'\"',
						'\'',
						'\''};
			this.fastColoredTextBox1.AutoScrollMinSize = new System.Drawing.Size(263, 22);
			this.fastColoredTextBox1.BackBrush = null;
			this.fastColoredTextBox1.CharHeight = 22;
			this.fastColoredTextBox1.CharWidth = 12;
			this.fastColoredTextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.fastColoredTextBox1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
			this.fastColoredTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fastColoredTextBox1.IsReplaceMode = false;
			this.fastColoredTextBox1.Location = new System.Drawing.Point(3, 3);
			this.fastColoredTextBox1.Name = "fastColoredTextBox1";
			this.fastColoredTextBox1.Paddings = new System.Windows.Forms.Padding(0);
			this.fastColoredTextBox1.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
			this.fastColoredTextBox1.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("fastColoredTextBox1.ServiceColors")));
			this.fastColoredTextBox1.Size = new System.Drawing.Size(836, 485);
			this.fastColoredTextBox1.TabIndex = 0;
			this.fastColoredTextBox1.Text = "fastColoredTextBox1";
			this.fastColoredTextBox1.Zoom = 100;
			// 
			// OnecCodeEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1014, 730);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.toolStrip1);
			this.Controls.Add(this.mainMenu);
			this.MainMenuStrip = this.mainMenu;
			this.Name = "OnecCodeEditor";
			this.Text = "OnecCodeEditor";
			this.mainMenu.ResumeLayout(false);
			this.mainMenu.PerformLayout();
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.contextTreeViewMenu.ResumeLayout(false);
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
			this.splitContainer2.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.contextTabMenu.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.contextTextTabMenu.ResumeLayout(false);
			this.tabControl2.ResumeLayout(false);
			this.tabPage3.ResumeLayout(false);
			this.tabPage3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fastColoredTextBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private FastColoredTextBoxNS.FastColoredTextBox fastColoredTextBox1;
		private System.Windows.Forms.ToolStripButton ToolMainPanBtnCloseAll;
		private System.Windows.Forms.ToolStripButton ToolMainPanBtnClose;
		private System.Windows.Forms.ToolStripButton ToolMainPanBtnSaveAll;
		private System.Windows.Forms.ToolStripButton ToolMainPanBtnSaveFile;
		private System.Windows.Forms.ToolStripButton toolMainPanBtnOpenFile;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.ToolStripMenuItem новыйToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
		private System.Windows.Forms.ToolStripMenuItem readmeToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem закладкиToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripMenuItem заменитьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem предыдущийToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem следующийToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem найтиВФайлахToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem найтиToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem вставитьToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem вырезатьToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem копироватьToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem добавитьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem закрытьВсеКромеТекущейToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem закрытьToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem повторитьToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem отменитьToolStripMenuItem1;
		private System.Windows.Forms.ContextMenuStrip contextTextTabMenu;
		private System.Windows.Forms.ContextMenuStrip contextTabMenu;
		private System.Windows.Forms.ContextMenuStrip contextTreeViewMenu;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.RichTextBox richTextBox1;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.TabControl tabControl2;
		private System.Windows.Forms.ToolStripMenuItem разборкаToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem платформаToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem unPackv8ToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem выполнитьНаПлатформе1сToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem выполнитьВКонсолиToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem существующаяПапкаToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem новаяПапкаToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem существующийЭлементToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem новыйЭлементToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem висстановитьВидToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem отдалитьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem приблизитьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem выделитьВсеToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem вставитьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem копироватьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem вырезатьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem повторитьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem отменитьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem закрытьВсеToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem закрытьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem отладкаToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem сборкаToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem переименоватьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem сохранитьВсеToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem сохранитьКакToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem новыйToolStripMenuItem;
		private System.Windows.Forms.TreeView treeView1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.ToolStripButton toolMainPanBtnNewFile;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem buildToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem projectToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.MenuStrip mainMenu;
			
	}
}
